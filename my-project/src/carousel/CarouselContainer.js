import React from 'react';
import { Carousel } from 'react-bootstrap';
import  './Carosuel.css';
import { connect } from "react-redux";

const mapStateToProps = (store) => {
  const { deal_initial_data } = store;
  return { deal_initial_data };
};

function CarouselContainer ({deal_initial_data}) {
  return (
    <div className='div-container'>
    <div className='carousel-container'>
    {deal_initial_data.map((item) => {
          const {
            id,
            name,
            description,
            new_price,
            previous_price,
            discount_percentage,
            image_1,
          } = item;
    return (
<Carousel pause={false}>
    <Carousel.Item interval={2000}>
    <div key={id} className="row align-items-center">
              <div className="item-container--discount_container">
                <p>{discount_percentage}</p>
              </div>
              <div className="item-container--image">
              <img src={image_1} alt="First Plate" />
              </div>
              <div className="col-md-py-2">
              <div className="item-container--name">
                <p>{name}</p>
              </div>
              <div className="item-container--description">
                <div className='item-container__description--para'>
                  <p>{description}</p>
                </div>
                <div className="item-container--description--p_n_d">
                  <span><p>Previous price</p>${previous_price}</span><p>
                    
                   <p1>Now</p1> ${new_price}
                  </p>
                </div>

                </div>
                <div className='item-container__rem_time_and_btn_container--btn'>
                  <p>View Deal</p>
                </div>
              </div>
            </div>
    </Carousel.Item>
    <Carousel.Item interval={2000}>
    <div key={id} className="row align-items-center">
              <div className="item-container--discount_container">
                <p>{discount_percentage}</p>
              </div>
              <div className="item-container--image">
              <img src={image_1} alt="Second Plate" />
              </div>
              <div className="col-md-py-2">
              <div className="item-container--name">
                <p>{name}</p>
              </div>
              <div className="item-container--description">
                <div className='item-container__description--para'>
                  <p>{description}</p>
                </div>
                <div className="item-container--description--p_n_d">
                  <span><h1>Pervious Price</h1>${previous_price}</span><p>
                    
                    <h1>Now</h1>${new_price}
                  </p>
                </div>

                </div>
                <div className='item-container__rem_time_and_btn_container--btn'>
                  <p>View Deal</p>
                </div>
              </div>
            </div>
    </Carousel.Item>
    <Carousel.Item interval={2000}>
    <div key={id} className="row align-items-center">
              <div className="item-container--discount_container">
                <p>{discount_percentage}</p>
              </div>

              <div className="item-container--image">
              <img src={image_1} alt="Third Plate" />
              </div>

              <div className="col-md-py-2">
              <div className="item-container--name">
                <p>{name}</p>
              </div>
              <div className="item-container--description">
                <div className='item-container__description--para'>
                  <p>{description}</p>
                </div>
                <div className="item-container--description--p_n_d">
                  <span><h1>Pervious Price</h1>${previous_price}</span><p>
                    
                    <h1>Now</h1>${new_price}
                  </p>
                </div>
                </div>
                <div className='item-container__rem_time_and_btn_container--btn'>
                  <p>View Deal</p>
                </div>
                </div>
            </div>
    </Carousel.Item>
</Carousel>
    );
    })}
</div>
</div>

  );
}

const mapDisPatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDisPatchToProps)(CarouselContainer);



