const popularData = [
    {
        "id":1,
        "name":"MaMaiya Lamb Bhutuwa ",
        "description": " Grilled lamb cooked with carrot, capsicum, onion, tomato ",
        "image_1":"https://res.cloudinary.com/dees63q5v/image/upload/v1621166631/img-1_ceeoex.png",
        "image_2":"",
        "previous_price": 21,
        "new_price":17.85,
        "discount_percentage": "-15%",
        "remaining_time":"102d 5h",
        "bought_number": 8,
        "heightlights": " Enjoy your delicious  MaMaiya Lamb Bhutuwa  marinated with differnet spices and herbs A  highly recommended to everyone  who loves meat.Lamb is the most expensive of the three types and in recent decades sheep meat is increasingly only retailed as lamb, sometimes stretching the accepted distinctions given above. The stronger-tasting mutton is now hard to find in many areas, despite the efforts of the Mutton Renaissance Campaign in the UK. In Australia, the term prime lamb is often used to refer to lambs raised for meat. Other languages, for example French, Spanish, Italian and Arabic, make similar or even more detailed, distinctions among sheep meats by age and sometimes by sex and diet, though these languages do not always use different words to refer to the animal and its meat — for example, lechazo in Spanish refers to meat from milk-fed (unweaned) lambs.",
        "details": " $18 for Lamb bhutwa  (Total value upto $21) $18 for chicken sekuwa  (Total value upto $21)",
        "fine_print": " Valid for 5 months Pay through card Receive your voucher after your order is confirmed One voucher for per person",
        "how_to_redeem": " Expiry: 90 days from purchase You’ll receive an e-mail to confirm reservation details Present printed or smartphone voucher on arrival Download free ClassiDeals app for Android and IOS",
        "about_company": "AusNep IT Solutions is an Australian based Company which provides quality products at competitive price.To provide ultimate benefit and satisfaction for our customers is our priority.We provide preventive maintenance for the products under warranty. At AusNep, we strive to give the best possible service for the Medical supplies and heavy amount of discount. Along with the Medical supplies we have quality fresh food to provide food & excellent eating experience,with reasonably price."
    },
    {
        "id":2,
        "name":"Chicken Biryani",
        "description": "Biryani is one of the more complicated rice dish.chicken Biryani is the craving food for all. Deep in number of spices , store to enhance its taste and cooked in a slow heat.",
        "image_1":"https://res.cloudinary.com/dees63q5v/image/upload/v1621166676/img-2_f8hpoy.png",
        "image_2":"",
        "previous_price": 18,
        "new_price":15.3,
        "discount_percentage":"-15%",
        "remaining_time":"102d 5h",
        "bought_number": 54,
        "heightlights": "  Biryani is a mouth-watering complete meal thats excellent for all events. Long grained rice,fragrant and chicken was very tender with spices.Taste the different flavours, rice cooked to perfection and the chicken succulent.",
        "details": " $15 for mouth watering  Chicken Biryani (Total value upto $18 )",
        "fine_print": " Valid for 5 months Pay through card Receive your voucher after your order is confirmed One voucher for per person",
        "how_to_redeem": " Expiry: 90 days from purchase You’ll receive an e-mail to confirm reservation details Present printed or smartphone voucher on arrival Download free ClassiDeals app for Android and IOS",
        "about_company": "AusNep IT Solutions is an Australian based Company which provides quality products at competitive price.To provide ultimate benefit and satisfaction for our customers is our priority.We provide preventive maintenance for the products under warranty. At AusNep, we strive to give the best possible service for the Medical supplies and heavy amount of discount. Along with the Medical supplies we have quality fresh food to provide food & excellent eating experience,with reasonably price."
    },
    {
        "id":3,
        "name":"Lockdown Offer",
        "description": " Forget your lockdown and enjoy you food . Momocha with achar and salad. These scrumptious juicy momocha are filled with minced veggies or meat and are put in dumpling steamer.",
        "image_1":"https://res.cloudinary.com/dees63q5v/image/upload/v1621166702/img-3_h6cfl8.png",
        "image_2":"",
        "previous_price": 8.5,
        "new_price":7.225,
        "discount_percentage":"-15%",
        "remaining_time":"102d 5h",
        "bought_number": 10,
        "heightlights": " These scrumptious juicy momos are filled with minced veggies or meat and are put in dumpling steam. Momocha which  taste will totally make you forget about your health-conscious thoughts. Sauce or jhol achar is thin in texture and have a bit of a hot, spicy and tangy twist.",
        "details": " $18 for Lamb bhutwa  (Total value upto $21) $18 for chicken sekuwa  (Total value upto $21)",
        "fine_print": " Valid for 5 months Pay through card Receive your voucher after your order is confirmed One voucher for per person",
        "how_to_redeem": " Expiry: 90 days from purchase You’ll receive an e-mail to confirm reservation details Present printed or smartphone voucher on arrival Download free ClassiDeals app for Android and IOS",
        "about_company": "AusNep IT Solutions is an Australian based Company which provides quality products at competitive price.To provide ultimate benefit and satisfaction for our customers is our priority.We provide preventive maintenance for the products under warranty. At AusNep, we strive to give the best possible service for the Medical supplies and heavy amount of discount. Along with the Medical supplies we have quality fresh food to provide food & excellent eating experience,with reasonably price."
    },
    
]

export default popularData;