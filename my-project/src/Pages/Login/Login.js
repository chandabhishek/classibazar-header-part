import React, { Component } from 'react';
import './Login.css';



class Login extends Component{
    state={
        email:'',
        pwd:''
    }

    handleChange = (e) =>{
        const {name,value} = e.target
        this.setState({[name]:value})
    }

    handleSubmit = (e) =>{
        e.preventDefault()
        this.props.isLogin(true)
    }
    
    render(){
        return(
            <div className='div-login'>
                <div className='div-login-logo'>
                     <img
                            src="https://deals.classibazaar.com.au/assets/images/classi-logo.png"
                             alt=""
                     />
                </div>

                <div className='input'>
                    <form onSubmit = {this.handleSubmit}>
                        <h1>Login Now</h1>
                        <input className='input_button' type='email' name='email' placeholder='Email' required onChange={this.handleChange}/>
                        <input className='input_button' type='password' name='pwd' placeholder='Password' required onChange={this.handleChange}/>
                        <button className='button' onSubmit={this.handleSubmit}>Log In</button>
                    </form>
                </div>
            </div>
        )
    }
}


export default Login;
